/**
 * Internal dependencies
 */
import {iconColor, icons} from '../../../util/icons.js';
import SC_Button from '../../components/sc-buttons/index.js';
import VisibleLabel from '../../components/visible-label/index.js';
import OpenLabel from '../../components/open-label/index.js';

/**
 * External dependencies
 */
import classNames from 'classnames';

/**
 * WordPress dependencies
 */
import {_x} from '@wordpress/i18n';
import {Fragment} from '@wordpress/element'
import {InnerBlocks, InspectorControls, MediaUpload, PlainText} from '@wordpress/block-editor';
import {PanelBody, PanelRow, TextControl} from '@wordpress/components';
import {registerBlockType} from '@wordpress/blocks';
import {dispatch} from '@wordpress/data';

/**
 * Block constants
 */
const name = 'faq-item';
const title = _x('FAQ Item', 'FAQ item title', 'structured-content');
const icon = {src: icons.faq, foreground: iconColor};

const keywords = [
    _x('faq question', 'faq question', 'structured-content'),
    _x('faq answer', 'faq answer', 'structured-content'),
    _x('structured-content', 'structured content element faq',
        'structured-content'),
];

const blockAttributes = {
    css_class: {
        type: 'string',
        default: '',
    },
    question: {
        type: 'string',
    },
    custom_title_id: {
        type: 'string',
        default: '',
    },
    imageID: {
        type: 'string',
    },
    imageAlt: {
        type: 'string',
    },
    thumbnailImageUrl: {
        type: 'string',
    },
    visible: {
        type: 'boolean',
        default: true,
    },
    open: {
        type: 'boolean',
        default: false,
    },
};

registerBlockType(`structured-content/${name}`, {
    title,
    icon,
    category: 'structured-content',
    keywords,

    attributes: blockAttributes,
    parent: ['structured-content/faq'],

    supports: {
        reusable: false,
        html: false,
        inserter: false,
    },

    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @param  root0
     * @param  root0.attributes
     * @param  root0.className
     * @param  root0.align
     * @param  root0.isSelected
     * @param  root0.setAttributes
     * @param  root0.clientId
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    edit: ({
               attributes,
               className,
               align,
               isSelected,
               setAttributes,
               clientId,
           }) => {
        const {
            css_class,
            question,
            custom_title_id,
            imageID,
            imageAlt,
            thumbnailImageUrl,
            visible,
            open,
            title_tag = '',
            summary = false,
            generate_title_id = false,
        } = attributes;

        function onImageSelect(imageObject) {
            setAttributes({
                imageID: imageObject.id,
                imageAlt: imageObject.alt,
                thumbnailImageUrl: imageObject.sizes.thumbnail.url,
            });
        }

        function onRemoveImage() {
            setAttributes({
                imageID: '',
                imageAlt: '',
                thumbnailImageUrl: '',
            });
        }

        const ALLOWED_BLOCKS = ['core/heading', 'core/paragraph', 'core/list'];

        const TEMPLATE = [
            [
                'core/paragraph',
                {
                    placeholder: _x('Enter your answer here', 'Answer the question.', 'structured-content'),
                },
            ],
        ];

        if (typeof title_tag === 'undefined') {
            setAttributes({title_tag: 'h2'});
        }

        if (typeof summary === 'undefined') {
            setAttributes({summary: false});
        }

        if (typeof generate_title_id === 'undefined') {
            setAttributes({generate_title_id: false});
        }

        return [
            <Fragment>
                {(isSelected && generate_title_id) && (
                    <Fragment>
                        <InspectorControls>
                            <PanelBody>
                                <PanelRow>
                                    <TextControl
                                        label={_x('Custom title ID', 'Custom title ID', 'structured-content')}
                                        className="w-100"
                                        value={custom_title_id}
                                        help={_x('Custom title ID for the FAQ title.', 'Custom title ID', 'structured-content')}
                                        onChange={(custom_title_id) => setAttributes(
                                            {custom_title_id})}
                                    />
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                    </Fragment>
                )}
                <section
                    className={classNames(
                        className,
                        align && `align${align}`,
                        'sc_card'
                    )}
                    style={{margin: '15px auto 0'}}
                >
                    <div className="sc_toggle-bar">
                        <div
                            onClick={() => setAttributes({visible: !visible})}
                            style={{float: 'left'}}
                        >
                            <VisibleLabel visible={visible}/>
                        </div>
                        {summary && (
                            <div
                                onClick={() => setAttributes({open: !open})}
                                style={{float: 'left', marginLeft: 16, marginRight: 'auto'}}
                            >
                                <OpenLabel open={open}/>
                            </div>
                        )}
                        <div onClick={() => dispatch('core/block-editor').removeBlocks(clientId)}>
                            {icons.remove}
                        </div>
                    </div>
                    <div>
                        {wp.element.createElement(title_tag, {className: 'question'},
                            <PlainText
                                placeholder={_x('Enter Your Question here',
                                    'Research a meaningful question', 'structured-content')}
                                value={question}
                                className="wp-block-structured-content-faq__title question"
                                tag={title_tag}
                                onChange={(question) => setAttributes(
                                    {question})}
                                keepplaceholderonfocus="true"
                            />,
                        )}
                        <div>
                            {!thumbnailImageUrl
                                ? <MediaUpload
                                    onSelect={(media) => onImageSelect(media)}
                                    type="image"
                                    value={imageID}
                                    render={({open}) => (
                                        <SC_Button action={open} className="inline">
                                            {_x('Add Image',
                                                'Illustrate your FAQ with a meaningful image.',
                                                'structured-content')}
                                        </SC_Button>
                                    )}
                                />
                                : <figure className="sc_fs_faq__figure" style={{
                                    position: 'relative',
                                    marginRight: 0,
                                    marginTop: 0,
                                }}>
                                    <a href="#" title={imageAlt}>
                                        <img
                                            itemProp="image"
                                            src={thumbnailImageUrl}
                                            alt={imageAlt}
                                        />
                                    </a>
                                    <SC_Button action={onRemoveImage}
                                               className="delete no-margin-top">
                                        {icons.close}
                                    </SC_Button>
                                </figure>
                            }
                            <div className="answer" itemProp="text">
                                <InnerBlocks
                                    allowedBlocks={ALLOWED_BLOCKS}
                                    template={TEMPLATE}
                                    templateInsertUpdatesSelection={false}
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>,
        ];
    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save: () => {
        return (
            <InnerBlocks.Content/>
        );
    },
});
