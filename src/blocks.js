/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './util/block-category.js';

import './blocks/faq/faq-item/index.js';
import './blocks/faq/index.js';
import './blocks/job/index.js';
import './blocks/event/index.js';
import './blocks/person/index.js';
import './blocks/course/index.js';
import './blocks/local-business/index.js';
