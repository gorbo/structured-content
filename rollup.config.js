import scss from 'rollup-plugin-scss';
import babel from '@rollup/plugin-babel';
import {nodeResolve} from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import {terser} from 'rollup-plugin-terser';
import postcss from 'rollup-plugin-postcss';
import replace from '@rollup/plugin-replace';

const createJSConfig = (input, output, useSass = false) => {
    return {
        input,
        output: {
            file: output,
            format: 'iife',
            plugins: [
                terser(),
            ],
        },
        external: ['tinymce'],
        plugins: [
            commonjs(),
            babel({
                presets: [
                    [
                        '@babel/preset-env',
                    ],
                ],
                plugins: [`@babel/plugin-transform-runtime`],
                exclude: 'node_modules/!**',
                babelHelpers: 'runtime',
            }),
            // https://github.com/rollup/rollup-plugin-node-resolve
            nodeResolve({
                browser: true,
            }),
            useSass
                ? scss({
                    // see options here : https://www.npmjs.com/package/node-sass
                    output: 'dist/blocks.style.build.css',
                    outputStyle: 'expanded',
                    sourceComments: true,
                    sourceMapEmbed: true,
                })
                : postcss({extensions: ['.css'], minimize: true}),
        ],
    };
};
const createJSXConfig = (input, output) => {
    return {
        input,
        output: {
            file: output,
            format: 'iife',
            plugins: [
                terser({
                    keep_fnames: true,
                }),
            ],
            globals: {
                react: 'React',
                'react-dom': 'ReactDOM',
                '@wordpress/block-editor': 'wp.blockEditor',
                '@wordpress/editor': 'wp.editor',
                '@wordpress/element': 'wp.element',
                '@wordpress/i18n': 'wp.i18n',
                '@wordpress/components': 'wp.components',
                '@wordpress/blocks': 'wp.blocks',
                '@wordpress/data': 'wp.data',
                '@wordpress/date': 'wp.date',
                lodash: 'lodash',
                wp: 'wp',
            },
        },
        external: ['wp', 'wp.element', 'wp.components', 'wp.blocks', 'wp.data', 'wp.i18n', 'wp.date', 'wp.blockEditor', 'wp.editor', '_'],
        plugins: [
            // https://github.com/rollup/rollup-plugin-node-resolve
            nodeResolve({
                browser: true,
            }),
            commonjs({
                include: /node_modules/,
            }),
            babel({
                presets: [
                    '@wordpress/babel-preset-default',
                    '@babel/preset-react',
                ],
                plugins: [
                    '@babel/plugin-transform-runtime'
                ],
                exclude: 'node_modules/!**',
                babelHelpers: 'runtime',
                compact: true,
            }),
            replace({
                'process.env.NODE_ENV': JSON.stringify('production'),
                preventAssignment: true,
            }),
            scss({
                // see options here : https://www.npmjs.com/package/node-sass
                output: 'dist/blocks.editor.build.css',
                outputStyle: 'expanded',
                sourceComments: true,
                sourceMapEmbed: true,
            }),
        ],
    };
};

export default [
    createJSConfig('./src/tinymce/tinymce.js', 'dist/tinymce.js'),
    createJSConfig('./src/app.js', 'dist/app.build.js', true),
    createJSXConfig('./src/blocks.js', 'dist/blocks.build.js'),
];
