<?php
/**
 * structured-content
 * faq.php
 *
 * @author antonioleutsch
 * @package  Default
 * @date     12.09.20 12:11
 * @version  GIT: 1.0
 */
foreach ( $atts['elements'] as $element ) {

	$title_ID = $element->atts->custom_title_id !== '' ? sanitize_title( $element->atts->custom_title_id ) : sanitize_title( $element->atts->question );

	$title = '<' . $atts['title_tag'] . ( $atts['generate_title_id'] ? ' id="' . $title_ID . '"' : '' ) . '>' . esc_attr( $element->atts->question ) . '</' . $atts['title_tag'] . '>';

	if ( $element->atts->visible ) { ?>
		<<?php echo $atts['summary'] ? 'details' : 'section'; ?>
		class="sc_fs_faq sc_card <?php echo $atts['css_class']; ?> <?php echo $atts['className']; ?> <?php echo $element->atts->className; ?> <?php echo $atts['summary'] && $atts['animate_summary'] ? ' sc_fs_card__animate' : ''; ?>"
		<?php echo $atts['summary'] && $element->atts->open ? 'open' : ''; ?>
		>
		<?php if ( $atts['summary'] ) { ?>
			<summary>
		<?php } ?>
		<?php echo $title; ?>
		<?php if ( $atts['summary'] ) { ?>
			</summary>
		<?php } ?>
		<div>
			<?php if ( $element->atts->thumbnailImageUrl ) { ?>
				<figure class="sc_fs_faq__figure">
					<a
							href="<?php echo $element->atts->thumbnailImageUrl; ?>"
							title="<?php echo $element->atts->imageAlt; ?>"
					>
						<img
								class="sc_fs_faq__image"
								src="<?php echo $element->atts->thumbnailImageUrl; ?>"
								alt="<?php echo $element->atts->imageAlt; ?>"
						>
					</a>
				</figure>
			<?php } ?>
			<div class="sc_fs_faq__content">
				<?php echo $element->content; ?>
			</div>
		</div>
		</<?php echo $atts['summary'] ? 'details' : 'section'; ?>>
		<?php
	}
}
?>

<script type="application/ld+json">
	{
		"@context": "https://schema.org",
		"@type": "FAQPage",
		"mainEntity": [
		<?php
		foreach ( $atts['elements'] as $element ) {
			// Remove not allowed Tags
			$content = strip_tags( $element->content, $allowedTags );
			// Remove Attributes from Tags
			$content = preg_replace( '/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si', '<$1$2>', $content );
			?>
			{
				"@type": "Question",
				"name": "<?php echo esc_attr( $element->atts->question ); ?>",
				"acceptedAnswer": {
					"@type": "Answer",
					"text": "<?php echo addcslashes( $content, '"\\' ); ?>"
					<?php if ( $element->atts->thumbnailImageUrl ) { ?>
					,
					"image" : {
						"@type" : "ImageObject",
						"contentUrl" : "<?php echo $element->atts->thumbnailImageUrl; ?>"
					}
					<?php } ?>
				}
			}
			<?php echo $element !== end( $atts['elements'] ) ? ',' : ''; ?>
	<?php } ?>
		]
	}








</script>
